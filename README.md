# Business Logic Command

The Business Logic Command helps to implement the business logics.

In the software development, the *business logic* implementation becomes in the gold for the developed product, because they defines the product life.

A poor implementation can produce a product with a short life.

## Configuration

### Spring version required

The library requires at least the Spring version `5.2.7.RELEASE`. 

### Maven dependencies

Add the library dependency

```xml
<dependency>
    <groupId>com.jatun.open.tools</groupId>
    <artifactId>business-logic-command</artifactId>
    <version>1.2.1-SNAPSHOT</version>
</dependency>
```

### Spring Boot

Import the library configuration class in Spring Boot application.

```java
@EnableAutoConfiguration
@ComponentScan
@Import({
        com.jatun.open.tools.blcmd.Config.class
})
public class AppConfig {

    public static void main(String[] args) {
        ...
    }

}
```

## BusinessLogicCommand Interface

All commands that implements `BusinessLogicCommand` interface are executed in synchronous way. Additionally, all commands should be annotated with `@CommandComponent` to be enabled in **Spring IOC** container under prototype scope.

### Transactions

A Spring transactional service is the responsible to execute this kind of commands. The transaction propagation is `Propagation.REQUIRED` 

### Implementation 

```java
@CommandComponent
public class CountryCreateCmd implements BusinessLogicCommand {

    private String name;

    private Country country;

    @Autowired
    private CountryRepository repository;

    @Override
    public void execute() {
        Country instance = new Country();
        instance.setName(name);

        country = repository.save(instance);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return this.country;
    }
}
```

### Usage

```java
@RestController
@RequestScope
public class CountryCreateController {

    @Autowired
    private CountryCreateCmd command;

    @PostMapping("/countries")
    public ResponseEntity<Country> create(@RequestBody CountryRequest requestBody) {
        command.setName(requestBody.getName());
        command.execute();

        Country country = command.getCountry();

        return ResponseEntity.status(HttpStatus.OK).body(country);
    }
}
```

