/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.jatun.open.tools.blcmd.exception;

/**
 * @author Ivan Alban
 */
public final class HandledBusinessLogicException extends Exception {

    private Throwable source;

    public HandledBusinessLogicException(Throwable cause) {
        super(cause);
        this.source = cause;
    }

    public Throwable getSource() {
        return source;
    }
}
