package com.jatun.open.tools.blcmd.interceptor;

import com.jatun.open.tools.blcmd.annotations.CommandSettings;
import com.jatun.open.tools.blcmd.annotations.Transaction;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

/**
 * @author Ivan Alban
 */
@Component
class ExecutorServiceLoader {

    Class<? extends BusinessLogicCommandExecutorService> load(Object commandInstance) {
        Class<?> commandClazz = commandInstance.getClass();

        CommandSettings settings = AnnotationUtils.findAnnotation(commandClazz, CommandSettings.class);
        if (null == settings) {
            return RequiredTransactionExecutorService.class;
        }

        if (Transaction.NO.equals(settings.transaction())) {
            return NoTransactionExecutorService.class;
        }

        if (Transaction.REQUIRES_NEW.equals(settings.transaction())) {
            return RequiredNewTransactionExecutorService.class;
        }

        if (Transaction.REQUIRED.equals(settings.transaction())) {
            return RequiredTransactionExecutorService.class;
        }

        throw new RuntimeException("Unable to load the ExecutorService for:" + commandClazz.getName());
    }
}
