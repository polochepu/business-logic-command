/*
 *
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.jatun.open.tools.blcmd.interceptor;

import com.jatun.open.tools.blcmd.annotations.BusinessLogicException;
import com.jatun.open.tools.blcmd.exception.HandledBusinessLogicException;

/**
 * @author Ivan Alban
 */
abstract class Work {

    public final void doWork() throws Throwable {
        try {
            preProceed();

            proceed();

            postProceed();
        } catch (Throwable throwable) {
            throw handleProceedException(throwable);
        }
    }

    protected abstract void proceed() throws Throwable;

    protected abstract void preProceed() throws Throwable;

    protected abstract void postProceed() throws Throwable;

    private Throwable handleProceedException(Throwable source) {
        if (source.getClass().isAnnotationPresent(BusinessLogicException.class)) {
            return new HandledBusinessLogicException(source);
        }

        return source;
    }
}
