package com.jatun.open.tools.test.blcmd.commons;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Ivan Alban
 */
@Component
@Scope("prototype")
public class PrototypeDependency {
}
