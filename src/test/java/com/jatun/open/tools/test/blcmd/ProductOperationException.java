package com.jatun.open.tools.test.blcmd;

import com.jatun.open.tools.blcmd.annotations.BusinessLogicException;

/**
 * @author Ivan Alban
 */
@BusinessLogicException
public class ProductOperationException extends RuntimeException {
}
