package com.jatun.open.tools.test.blcmd.spring;

import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import com.jatun.open.tools.test.blcmd.AbstractTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Ivan Alban
 */
public class ExecutionCycleTest extends AbstractTest {

    @Autowired
    private ExecutionCycleCmd command;

    @Autowired
    private BusinessLogicCommandFactory factory;

    @Test
    public void autowiredAnnotatedTest() {
        command.execute();
    }

    @Test
    public void commandFactoryTest() {
        ExecutionCycleCmd instance = factory.createInstance(ExecutionCycleCmd.class);

        instance.execute();
    }
}
