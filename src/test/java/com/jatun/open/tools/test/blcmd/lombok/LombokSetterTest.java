package com.jatun.open.tools.test.blcmd.lombok;

import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import com.jatun.open.tools.test.blcmd.AbstractTest;
import com.jatun.open.tools.test.blcmd.commons.Person;
import com.jatun.open.tools.test.blcmd.commons.SingletonDependency;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Ivan Alban
 */
public class LombokSetterTest extends AbstractTest {

    @Autowired
    private SingletonDependency singletonDependency;

    @Autowired
    private LombokSetterCmd command;

    @Autowired
    private BusinessLogicCommandFactory factory;

    @Test
    public void autowiredAnnotatedTest() {
        command.setEntity(new Person());
        command.setSingletonDependency(singletonDependency);
        command.setValue("default");

        command.execute();
    }

    @Test
    public void commandFactoryTest() {
        LombokSetterCmd instance = factory.createInstance(LombokSetterCmd.class);
        instance.setEntity(new Person());
        instance.setSingletonDependency(singletonDependency);
        instance.setValue("default");

        instance.execute();
    }
}
