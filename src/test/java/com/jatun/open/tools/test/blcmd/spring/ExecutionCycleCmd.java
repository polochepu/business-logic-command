package com.jatun.open.tools.test.blcmd.spring;

import com.jatun.open.tools.blcmd.annotations.CommandComponent;
import com.jatun.open.tools.blcmd.annotations.PostExecute;
import com.jatun.open.tools.blcmd.annotations.PreExecute;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.test.blcmd.commons.Entity;
import com.jatun.open.tools.test.blcmd.commons.Organization;
import com.jatun.open.tools.test.blcmd.commons.Person;
import com.jatun.open.tools.test.blcmd.commons.SingletonDependency;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Ivan Alban
 */
@CommandComponent
public class ExecutionCycleCmd implements BusinessLogicCommand {

    @Autowired
    private SingletonDependency singletonDependency;

    private Entity entity;

    @Override
    public void execute() {
        Assert.assertNotNull(entity);
        Assert.assertTrue(entity instanceof Person);

        entity = new Organization();
    }

    @PreExecute
    void onPreExecute(){
        Assert.assertNotNull(singletonDependency);
        Assert.assertNull(entity);

        entity = new Person();
    }

    @PostExecute
    void onPostExecute(){
        Assert.assertTrue(entity instanceof Organization);
    }
}
