package com.jatun.open.tools.test.blcmd.spring;

import com.jatun.open.tools.blcmd.annotations.CommandComponent;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.test.blcmd.commons.Entity;
import com.jatun.open.tools.test.blcmd.commons.OrganizationEntity;
import com.jatun.open.tools.test.blcmd.commons.SingletonDependency;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ivan Alban
 */
@CommandComponent
public class BeanLifeCycleCmd implements BusinessLogicCommand {

    @Autowired
    @OrganizationEntity
    private Entity organization;

    private SingletonDependency singletonDependency;

    private List<Entity> persons;

    @Override
    public void execute() {
        Assert.assertNotNull(persons);
        Assert.assertTrue(persons.isEmpty());
    }

    @PostConstruct
    void onPostConstruct() {
        Assert.assertNotNull(organization);
        Assert.assertNull(singletonDependency);
        Assert.assertNull(persons);

        persons = new ArrayList<>();
    }

    public void setSingletonDependency(SingletonDependency singletonDependency) {
        this.singletonDependency = singletonDependency;
    }

    public void setPersons(List<Entity> persons) {
        this.persons = persons;
    }
}
