package com.jatun.open.tools.test.blcmd.lombok;

import com.jatun.open.tools.blcmd.annotations.CommandComponent;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.test.blcmd.commons.Entity;
import com.jatun.open.tools.test.blcmd.commons.SingletonDependency;
import lombok.Setter;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Ivan Alban
 */
@CommandComponent
public class LombokSetterCmd implements BusinessLogicCommand {

    @Setter
    private SingletonDependency singletonDependency;

    @Setter
    private Entity entity;

    @Setter
    private String value;

    @Autowired
    private SingletonDependency singletonAutowiredDependency;

    @Override
    public void execute() {
        Assert.assertEquals(singletonAutowiredDependency, singletonAutowiredDependency);
        Assert.assertNotNull(entity);
    }
}
