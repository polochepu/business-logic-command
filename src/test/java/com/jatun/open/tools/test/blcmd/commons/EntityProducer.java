package com.jatun.open.tools.test.blcmd.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Ivan Alban
 */
@Component
class EntityProducer {

    @Bean
    @PersonEntity
    @Scope("prototype")
    Entity person() {
        return new Person();
    }

    @Bean
    @OrganizationEntity
    Organization organization() {
        return new Organization();
    }
}
