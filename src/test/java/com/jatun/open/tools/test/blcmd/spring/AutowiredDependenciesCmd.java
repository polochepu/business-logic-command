package com.jatun.open.tools.test.blcmd.spring;

import com.jatun.open.tools.blcmd.annotations.CommandComponent;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.test.blcmd.commons.*;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Ivan Alban
 */
@CommandComponent
public class AutowiredDependenciesCmd implements BusinessLogicCommand {

    @Autowired
    private PrototypeDependency prototypeDependency;

    @Autowired
    private SingletonDependency singletonDependency;

    @Autowired
    @PersonEntity
    private Entity person;

    @Autowired
    @OrganizationEntity
    private Entity organization;

    @Override
    public void execute() {
        Assert.assertNotNull(prototypeDependency);
        Assert.assertNotNull(singletonDependency);
        Assert.assertNotNull(person);
        Assert.assertNotNull(organization);
    }
}
