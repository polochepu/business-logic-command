package com.jatun.open.tools.test.blcmd.lombok;

import com.jatun.open.tools.blcmd.annotations.CommandComponent;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.test.blcmd.commons.Entity;
import com.jatun.open.tools.test.blcmd.commons.Person;
import com.jatun.open.tools.test.blcmd.commons.PrototypeDependency;
import com.jatun.open.tools.test.blcmd.commons.SingletonDependency;
import lombok.Getter;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Ivan Alban
 */
@CommandComponent
public class LombokGetterCmd implements BusinessLogicCommand {

    @Getter
    @Autowired
    private SingletonDependency singletonDependency;

    @Getter
    @Autowired
    private PrototypeDependency prototypeDependency;

    @Getter
    private Entity entity;

    @Getter
    private String value;

    @Override
    public void execute() {
        Assert.assertNotNull(singletonDependency);

        entity = new Person();

        value = "default";
    }
}
