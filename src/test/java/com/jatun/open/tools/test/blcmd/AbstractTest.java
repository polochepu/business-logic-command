package com.jatun.open.tools.test.blcmd;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Ivan Alban
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                com.jatun.open.tools.test.blcmd.Config.class
        }
)
public abstract class AbstractTest {
}
